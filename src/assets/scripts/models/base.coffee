window.iQuizTables = window.iQuizTables ? {
	Views: {}
	Collections: {}
	Models: {}
	Templates: {}
}


class iQuizTables.Models.Base extends Backbone.Model
	defaults:
		area: 1

	initialize: (attrs, options) ->
