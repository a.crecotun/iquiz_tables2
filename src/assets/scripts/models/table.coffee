window.iQuizTables = window.iQuizTables ? {
	Views: {}
	Collections: {}
	Models: {}
	Templates: {}
}


class iQuizTables.Models.Table extends Backbone.Model

	defaults:
		team: ''
		isReserved: false

	initialize: (attrs, options) ->
