window.iQuizTables = window.iQuizTables ? {
	Views: {}
	Collections: {}
	Models: {}
	Templates: {}
}

class iQuizTables.Views.Base extends Backbone.View

	initialize: (options) ->
		@collection = options.collection
		@gameName = options.gameName
		@gameId = options.gameId

		@tables = []
		@addStickit()
		@addListeners()

		@cacheDOM()

		@start()
		@bind()

		@stickit()

	bind: ->
		@$el.on 'click', (e) =>
			$target = $(e.target)
			$form = $('.iquiz_tables--form')
			$info = $('.iquiz_tables--info_popup')
			$opener = $('.iquiz_tables--table_opener')
			if (!$form.is(e.target) && $form.has(e.target).length is 0 && !$opener.is(e.target) && $opener.has(e.target).length is 0)
				@addForm.close()

			if (!$info.is(e.target) && $info.has(e.target).length is 0 && !$opener.is(e.target) && $opener.has(e.target).length is 0)
				@infoPopup.close()

	addListeners: ->
		@listenTo iQuizTables, 'loading', =>
			@showLoading()
		@listenTo iQuizTables, 'ready', =>
			@hideLoading()

	addStickit: ->
		@bindings =
			'[name="select_area"]':
				observe: 'area'
				onSet: (val) =>
					@$el.$area
						.removeClass('-area_1 -area_2')
						.addClass('-area_'+ val)
					return val

	cacheDOM: ->
		@$el.$inner = $('.iquiz_tables--inner')
		@$el.$area = $('.iquiz_tables--area')
		@$el.$loading = $('.iquiz_tables--loading')

	addOrgsTable: ->
		if @gameName is 'qnq'
			title = 'Организаторы'
		else
			title = 'Organizatori'

		@$el.$area.append( iQuizTables.Templates.TableOrgs({
			title: title
			}) )

	start: ->
		@addForm = new iQuizTables.Views.Form
			el: $('.iquiz_tables--form')
			model: new iQuizTables.Models.Form 0,
				gameName: @gameName
				gameId: @gameId

		@infoPopup = new iQuizTables.Views.InfoPopup
			el: $('.iquiz_tables--info_popup')

		@$el.addClass('-' + @gameName)

		@addOrgsTable()
		@collection.forEach (model) => @renderTable(model)

	renderTable: (model) ->
		@tables.push table = new iQuizTables.Views.Table
			model: model
			baseView: @
			addForm: @addForm
			infoPopup: @infoPopup

		@$el.$area.append( table.render().el )

		return @

	showLoading: ->
		@$el.$loading.show()

	hideLoading: ->
		@$el.$loading.hide()
