window.iQuizTables = window.iQuizTables ? {
	Views: {}
	Collections: {}
	Models: {}
	Templates: {}
}

class iQuizTables.Collections.Tables extends Backbone.Collection

	initialize: (models, options) ->
		@gameName = options.gameName
		@gameId = options.gameId

		@setTablesLength()
		@createEmpty()

		@fetch()
		@addListeners()

	setTablesLength: ->
		if @gameName is 'qnq'
			@tablesLength = 31
		else
			@tablesLength = 10

	createEmpty: ->
		for i in [1..@tablesLength]
			@add
				table_id: i

	fetch: ->
		iQuizTables.trigger 'loading'

		$.ajax
			method: 'post'
			url: 'http://cq.md/iquiz_tables/tables/backend/get_tables.php',
			data: JSON.stringify
				gameNum: @gameId
				gameType: @gameName

			success: (response) =>
				@setTables(JSON.parse(response))

				@trigger 'fetched'
				iQuizTables.trigger 'ready'

	setTables: (tables) ->
		tables.forEach (table) =>
			model = @find({table_id: +table.table_id})
			model?.set
				isReserved: true
				team: table.team

	addListeners: ->
		@listenTo iQuizTables, 'table:saved', @fetch
