import gulp from 'gulp'
import util from 'gulp-util'
import stylus from 'gulp-stylus'
import plumber from 'gulp-plumber'
import changed from 'gulp-changed'
import browserSync from 'browser-sync'
import bower from 'gulp-bower'
import ignore from 'gulp-ignore'
import coffee from 'gulp-coffee'
import concat from 'gulp-concat'

let bSync = browserSync.create();

const paths = {

	// source
	src: {
		styles: {
			all: './src/assets/styles/**/*.styl',
			main: './src/assets/styles/main.styl'
		},

		templates: {
			all: './src/**/*.html'
		},

		images: {
			all: './src/assets/images/**/*.*'
		},

		scripts: {
			all: './src/assets/scripts/**/*.coffee'
		}

	},

	// dist
	dist: {
		root: './dist/',

		styles: {
			root: './dist/assets/styles/',
			all: './dist/assets/styles/**/*.css'
		},

		images: {
			root: './dist/assets/images/',
			all: './dist/assets/images/**/*.*'
		},

		scripts: {
			root: './dist/assets/scripts/',
			all: './dist/assets/scripts/**/*.js'
		},

		templates: {
			root: './dist/',
			all: './dist/**/*.html'
		},

		libs: {
			root: './dist/assets/libs/'
		}

	}
}

// # Play sound if gulp got an error
function consoleErorr(err) {
	util.beep()
	console.log( err.message )
}

gulp.task('clean', () => {
	return gulp.src( paths.dist.root , { read: false } )
		.pipe( rimraf() )
});

// compile styl files to css
gulp.task('styles', () => {
	return gulp.src( paths.src.styles.main )
		.pipe( plumber({
			errorHandler: consoleErorr
		}) )
		.pipe( stylus() )
		.pipe( gulp.dest( paths.dist.styles.root ) );
});


// move images from src to dist folder
gulp.task('images', () => {
	return gulp.src( paths.src.images.all )
		.pipe( plumber({
			errorHandler: consoleErorr
		}) )
		.pipe( gulp.dest( paths.dist.images.root ) );
});

gulp.task('scripts', () => {
	return gulp.src( paths.src.scripts.all )
		.pipe( plumber({
			errorHandler: consoleErorr
		}) )
		.pipe( changed( paths.dist.scripts.root, { extension: '.js' } ) )
		.pipe( coffee() )
		.pipe( gulp.dest( paths.dist.scripts.root ) );
});

// create server and start watching files chages
gulp.task('browser-sync', () => {
	return bSync.init({
		minify: false,
		injectChanges: true,
		files: [ paths.dist.styles.all, paths.dist.scripts.all ],
		notify: false,
		server: {
			baseDir: '.'
		},
		port: 3010
	});
});

gulp.task('bower', () => {
	return bower( paths.dist.libs.root )
});

gulp.task('concat', () => {

	return gulp.src([
		'./dist/assets/libs/underscore/underscore-min.js',
		'./dist/assets/libs/backbone/backbone-min.js',
		'./dist/assets/libs/handlebars/handlebars.min.js',
		'./dist/assets/libs/backbone.validation/dist/backbone-validation-min.js',
		'./dist/assets/libs/backbone.stickit/backbone.stickit.js',
		'./dist/assets/scripts/views/base.js',
		'./dist/assets/scripts/views/form.js',
		'./dist/assets/scripts/views/table.js',
		'./dist/assets/scripts/views/info_popup.js',
		'./dist/assets/scripts/models/base.js',
		'./dist/assets/scripts/models/table.js',
		'./dist/assets/scripts/models/form.js',
		'./dist/assets/scripts/collections/tables.js',
		'./dist/assets/scripts/app.js',
	])
		.pipe( concat('iquiz_tables.js') )
		.pipe( gulp.dest( paths.dist.scripts.root ) );


});

// watch for files changes
gulp.task('watch', () => {
	gulp.watch( paths.src.styles.all, ['styles'] );
	gulp.watch( paths.src.images.all, ['images'] );
	gulp.watch( paths.src.scripts.all, ['scripts'] );
	gulp.watch( paths.dist.scripts.all, ['concat'] );
});

gulp.task('default', ['styles', 'images', 'bower', 'scripts']);

gulp.task('dev', ['default', 'browser-sync', 'watch']);
