<?php
	$iq_game = array(
		'num' => $game['nomer'],
		'type' => $game['type']
	);
?>

<div class="iquiz_tables" data-options='{
	"gameName": "<?=$iq_game['type']?>",
	"gameId": "<?=$iq_game['num']?>"
}'>
  <div class="iquiz_tables--loading">Loading</div>
	<div class="iquiz_tables--inner">
		<div class="iquiz_tables--area -area_1">
			<form action="#" class="iquiz_tables--form iquiz_tables--popup">
				<fieldset>
					<input type="text" placeholder="Команда" name="team">
					<input type="text" placeholder="Капитан" name="captain">
					<input type="text" placeholder="Телефон" name="phone">
					<button type="submit">Зарезервировать</button>
				</fieldset>
			</form>
			<div class="iquiz_tables--info_popup iquiz_tables--popup">
				<div class="iquiz_tables--table_info"></div>
			</div>
		</div>
	</div>
	<nav class="iquiz_tables--nav">
		<label>
			<input type="radio" name="select_area" value="1">
			<span>1</span>
		</label>
		<label>
			<input type="radio" name="select_area" value="2">
			<span>2</span>
		</label>
	</nav>
</div>

<script id="table-template" type="text/x-handlebars-template">
	<div class="iquiz_tables--table_inner">
		<button class="iquiz_tables--table_opener" type="button">
			<strong class="iquiz_tables--table_num">{{table_id}}</strong>
		</button>
	</div>
</script>

<script id="org-table-template" type="text/x-handlebars-template">
	<div class="iquiz_tables--table -org">
		<div class="iquiz_tables--table_inner">
			<span class="iquiz_tables--table_org">{{title}}</span>
		</div>
	</div>
</script>

<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="/iquiz_tables/tables/dist/assets/styles/main.css">

<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="/iquiz_tables/tables/dist/assets/libs/underscore/underscore-min.js"></script>
<script src="/iquiz_tables/tables/dist/assets/libs/handlebars/handlebars.min.js"></script>
<script src="/iquiz_tables/tables/dist/assets/libs/backbone/backbone-min.js"></script>
<script src="/iquiz_tables/tables/dist/assets/libs/backbone.validation/dist/backbone-validation-min.js"></script>
<script src="/iquiz_tables/tables/dist/assets/libs/backbone.stickit/backbone.stickit.js"></script>

<script src="/iquiz_tables/tables/dist/assets/scripts/iquiz_tables.js"></script>
